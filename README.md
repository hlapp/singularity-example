# Singularity Container Example

This example project gives the steps needed to create a Singularity container for use in the Duke Compute Cluster (DCC).

* Create a project in GitLab.
    * If you don't have an account on https://gitlab.oit.duke.edu, going to the URL in your browser and clicking on the "Duke Shibboleth Login" button will automatically log you in and create an account for you.
    * If you're working in a research group you should create a GitLab group so that team members can use share your projects. If another team member has already created a group, once you have an account have them add you as a member. The person to create a group owns it, but can add other users as members and/or grant privileges such developer or master to the group. If this is a standalone project you can just create it under your user name.
    * As a point of information, the name of the project you create will also be the name of your Singularity container image.

* Create a _Singularity.def_ file and add it to your project. The example in the _OIT-DCC/singularity-example_ project uses a Docker image of Anaconda Python 3 with the Caffe module installed. **One important note:** Notice in the _%post_ section a **mkdir /dscrhome** is done. You need to add this to any _Singularity.def_ file you create for use in the DCC. Singularity passes in the home directory of the user by default as a bind mount. The DCC uses /dscrhome instead of the standard /home so this mount point will not exist in any of the images unless you create it. Likewise, if you want to pass in a /datacommons mount you will need to make that directory mount point as well.

* Once you have your project created you will need to add a tag to the project that tells GitLab which CI runner to use to build your Singularity image. Click on _Settings_ and scroll down to the _Tags_ field and add **dcc**. Click the "_Save Changes_" button to save your tag. 

* Create a _.gitlab-ci.yml_ file to run the Continuous Integration pipeline. The contents of the file should match the contents of the _.gitlab-ci.yml_ file in the _OIT-DCC/singularity-example_ project. Once this file is added to your project every time you push a change to your project a CI build of your Singularity image will be done and the container results placed on the Singularity Registry server.

* The Singularity images created are stored on research-singularity-registry.oit.duke.edu. You can pull your image down to the DCC by logging into one of the DCC slogin servers (dcc-slogin-01/dcc-slogin-02) and pulling your image down by _curl_ (_e.g. curl -O https://research-singularity-registry.oit.duke.edu/OIT-DCC/singularity-example.tar.gz_). You can the extract the image by _tar xf {project name}.tar.gz.

* To use the image you would include ""singularity exec"" or "singularity run", whichever your Singularity.def file has set up in your submit scripts. For example:
``` YAML
# !/Bin/Bash
# SBATCH -E slurs.err
singularity exec {your project}.img some_script.py
```


